﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    public AudioClip dia;
    public AudioClip noche;

    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        PlayDay();
    }

    public void PlayDay()
    {
        audioSource.clip = dia;
        audioSource.Play();
    }

    public void PlayNight()
    {
        audioSource.Stop();
        audioSource.clip = noche;
        audioSource.Play();
    }
}
