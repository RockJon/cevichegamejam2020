﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueController : MonoBehaviour
{
    [Header("Text Obj")]
    [SerializeField]
    Text textObj;

    [Header("Title Obj")]
    [SerializeField]
    Text titleObj;

    public static DialogueController instance;


    public bool isShowing = false;
    public bool animShowEnd = false;

    private Animator anim;

    private void Awake()
    {
        if (instance == null)
        {

            instance = this;
            // DontDestroyOnLoad(this.gameObject);
            //Rest of your Awake code
        }
        else
        {
            Destroy(this);
        }
        anim = gameObject.GetComponent<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {
       
    }
    private void Update()
    {
        // print(animShowEnd);
        if (isShowing)
            if (Input.GetMouseButton(0))
                HideDialog();
    }

    public void ShowDialoge(string _title, string _text, float _time)
    {
        //en caso no haya titulo colocar un espacio " "

        //Block interactions while text is showing?
        if (isShowing)
            return;
        isShowing = true;

        titleObj.text = _title;
        textObj.text = _text;
        //turn on
        anim.SetTrigger("Show"); //this will automatically show the text.
        //Invoke("HideDialog", _time); //deactivating timer
    }

    void HideDialog()
    {
        anim.SetTrigger("Hide");
        animShowEnd = false;
    }

    void RestartBool()
    {
        isShowing = false;
        anim.ResetTrigger("Hide");
    }

    void ShowEnds()
    {
        animShowEnd = true;
    }

}
