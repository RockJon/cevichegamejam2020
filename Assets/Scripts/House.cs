﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class House : MonoBehaviour
{
    public StyleName style;
    Button button;

    void Awake()
    {
        button = GetComponent<Button>();
    }

    void Start()
    {
        button.onClick.AddListener(() => GameManager.inst.HouseClicked(style));

        var eventTrigger = GetComponent<EventTrigger>();
        var outline = GetComponent<Outline>();

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerEnter;
        entry.callback.AddListener((data) => { GameManager.inst.ToggleOutline(outline, true); });
        eventTrigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerExit;
        entry.callback.AddListener((data) => { GameManager.inst.ToggleOutline(outline, false); });
        eventTrigger.triggers.Add(entry);
    }
}
