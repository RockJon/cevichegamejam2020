﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    #region TYPES
    public enum Phase { Investigation, Matching }

    [System.Serializable]
    public class UIHouse
    {
        public StyleName style;
        public RectTransform poolContainer;
        public RectTransform house;
        public Image costumeImage;
        public RectTransform costumeRect;
        public Sprite nightHouseSprite;

        [NonSerialized] public bool shownAlready;
        [NonSerialized] public Vector2 position;
        [NonSerialized] public Vector2 size;
        [NonSerialized] public List<StyleName> objsStyles;
        [NonSerialized] public StyleName costumeStyle = StyleName.None;
    }

    [System.Serializable]
    public class UICostume
    {
        public StyleName name;
        public Image image;
        public Button button;
    }

    [System.Serializable]
    public class ScoreKittyPaw
    {
        public RectTransform rt;
        public Image image;
        public Sprite completedSprite;
    }
    #endregion


    public static bool showMainMenu = true;
    public static GameManager inst;


    [Header("UI")]
    public GameObject mainMenuScreen;

    [Header("Investigation")]
    public DialogueController dialogueController;
    public GameData data;
    public Text checkedObjsText;
    public GameObject houseInteriorContainer;
    public GameObject objStylePrefab;
    public List<UIHouse> uiHouses;
    public Image houseInteriorBackground;
    public Color outlineColor;
    public Image overlay;


    [Header("Transition")]
    public float timeToStartTransition;
    public float witchAnimLength;
    public Ease witchAnimEaseSizeEase;
    public AnimationCurve witchAnimEaseXEase, witchAnimEaseYEase;
    public Vector2 witchAnimTargetPos;
    public Vector2 witchAnimSize;
    public RectTransform witch;
    public float fadeAnimDelay;
    public float fadeAnimLength;
    public Ease fadeAnimEase;
    public float fadeThunderBlackAnimLength;
    public AnimationCurve fadeThunderBlackAnimCurve;
    public float thunderDelays;
    public GameObject[] thunders;
    public AudioSource audioSourceBGM;
    public Image gameplayBackground;
    public Sprite nightBackgroundSprite;


    [Header("Matching")]
    public GameObject matchingContainer;
    public Text scoreText;
    public GameObject submitButton;
    public GameObject scorePanel;
    public List<UICostume> uICostumes;
    public ScoreKittyPaw[] paws;
    public float pawDelay;


    [Header("SFX")]
    public AudioSource audioSourceSFX;
    public AudioClip buttonClickedSFX, styleObjClickedSFX, transitionWitchSFX, houseClickedSFX, scoreSFX;
    public AudioClip[] kittyMeowSFX;


    Phase currentPhase;
    StyleName selectedCostume;
    int selectedCostumeIndex;
    GameObject lastInteriorPool;
    int objsToCheck;
    bool initialDialoge = true;

    void Awake()
    {
        if (inst == null)
            inst = this;

        mainMenuScreen.SetActive(showMainMenu);
    }

    void Start()
    {
        foreach(var style in data.styles)
        {
            style.SetObjsStyleName();
        }

        for(int i=0 ; i<uiHouses.Count ; i++)
        {
            uiHouses[i].objsStyles = new List<StyleName>();
            uiHouses[i].costumeStyle = StyleName.None;
            uiHouses[i].position = uiHouses[i].house.anchoredPosition;
            uiHouses[i].size = uiHouses[i].house.sizeDelta;
        }

        selectedCostume = StyleName.None;
        currentPhase = Phase.Investigation;
        objsToCheck = data.maxCheckableObjs;

        // objsToCheck = 1;

        checkedObjsText.text = objsToCheck.ToString();

        // TransitionToNextPhase();

    }

    public void StartInitialDialogue()
    {
        StartCoroutine(InitialDialogueCoroutine());
    }

    public IEnumerator InitialDialogueCoroutine()
    {
        DialogueController.instance.ShowDialoge("", "It's halloween day today folks! get your paws ready for unlimited candy!  ", 0.0f);

        yield return new WaitUntil(()=>DialogueController.instance.isShowing == false);

        DialogueController.instance.ShowDialoge("", "We need to do better this year, so we need to make up a plan", 0.0f);

        yield return new WaitUntil(() => DialogueController.instance.isShowing == false);

        DialogueController.instance.ShowDialoge("", "I know! How about we spy our neighbours and find out what they like.", 0.0f);

        yield return new WaitUntil(() => DialogueController.instance.isShowing == false);

        DialogueController.instance.ShowDialoge("", "So when the night comes, we'll ring their bells and THEY WILL LOVE OUR OUTFITS!!", 0.0f);

        yield return new WaitUntil(() => DialogueController.instance.isShowing == false);

        initialDialoge = false;

        yield return null;
    }

    public void ShowMap()
    {
        lastInteriorPool.SetActive(false);
        houseInteriorContainer.SetActive(false);

        if (objsToCheck == 0)
            TransitionToNextPhase();
    }

    public void ShowHouse(StyleName styleName)
    {
        // Debug.Log(style);

        var styleData = data.styles.Find((style) => style.name == styleName);
        var uiHouse = uiHouses.Find((house) => house.style == styleName);

        houseInteriorBackground.sprite = styleData.background;
        lastInteriorPool = uiHouse.poolContainer.gameObject;
        lastInteriorPool.SetActive(true);
        houseInteriorContainer.SetActive(true);

        if (uiHouse.shownAlready)
            return;

        uiHouse.shownAlready = true;
        List<StyleObject> mainStyleObjs = new List<StyleObject>(styleData.objects);

        //Listing all style objs except the house main style objs
        List<StyleObject> otherObjs = new List<StyleObject>();
        foreach(var style in data.styles)
        {
            if (style.name == styleName)
                continue;

            otherObjs.AddRange(style.objects);
        }

        //Randomizing objs to be added to the house using the main style
        List<StyleObject> houseObjs = new List<StyleObject>();
        do
        {
            int objIndex = 0;
            if (houseObjs.Count < data.mainStyleObjs)
            {
                objIndex = Random.Range(0, mainStyleObjs.Count);
                houseObjs.Add(mainStyleObjs[objIndex]);
                mainStyleObjs.RemoveAt(objIndex);
            }
            else
            {
                objIndex = Random.Range(0, otherObjs.Count);
                houseObjs.Add(otherObjs[objIndex]);
                otherObjs.RemoveAt(objIndex);
            }

        } while (houseObjs.Count < data.maxObjsPerHouse);


        //Randomizing objs positions
        List<Vector3> objsPositions = new List<Vector3>();
        List<Vector3> allPositions = new List<Vector3>(styleData.objectsPositions);
        do
        {
            int randomIndex = Random.Range(0, allPositions.Count);
            objsPositions.Add(allPositions[randomIndex]);
            allPositions.RemoveAt(randomIndex);
        } while (objsPositions.Count < data.maxObjsPerHouse);


        //Instantiating objs
        int posIndex = 0;
        foreach(var styleObj in houseObjs)
        {
            uiHouse.objsStyles.Add(styleObj.styleName);

            var gb = GameObject.Instantiate(objStylePrefab, objsPositions[posIndex],
                transform.rotation, uiHouse.poolContainer);

            var gbRect = gb.GetComponent<RectTransform>();
            gbRect.anchoredPosition = objsPositions[posIndex];
            gbRect.sizeDelta = styleObj.sprite.textureRect.size;

            posIndex++;

            var image = gb.GetComponent<Image>();
            image.sprite = styleObj.sprite;

            var button = gb.GetComponent<Button>();
            button.onClick.AddListener(() =>
            {
                // Debug.Log($"{objsToCheck} {DialogueController.instance.isShowing}");
                if (objsToCheck == 0 || DialogueController.instance.isShowing)
                    return;

                DialogueController.instance.ShowDialoge(styleObj.title, styleObj.description, 3f);

                objsToCheck--;
                if (objsToCheck > 0)
                    checkedObjsText.text = objsToCheck.ToString();
                else
                    checkedObjsText.gameObject.SetActive(false);

                button.enabled = false;

                audioSourceSFX.PlayOneShot(styleObjClickedSFX);
            });


            var eventTrigger = gb.GetComponent<EventTrigger>();
            var outline = gb.GetComponent<Outline>();

            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
            entry.callback.AddListener((data) =>
            {
                if (button.enabled && !DialogueController.instance.isShowing)
                    ToggleOutline(outline, true);
            });
            eventTrigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerExit;
            entry.callback.AddListener((data) => ToggleOutline(outline, false));
            eventTrigger.triggers.Add(entry);
        }
    }

    public void ToggleOutline(Outline outline, bool show)
    {
        // Debug.Log(show);
        Color targetColor = show? outlineColor : Color.clear;
        outline.DOKill();
        outline.DOColor(targetColor, 1f);
    }

    public void TransitionToNextPhase()
    {
        currentPhase = Phase.Matching;
        audioSourceSFX.PlayOneShot(transitionWitchSFX);
        audioSourceBGM.GetComponent<Audio>().PlayNight();
        overlay.raycastTarget = true;

        witch.DOAnchorPosX(witchAnimTargetPos.x, witchAnimLength).SetDelay(timeToStartTransition).SetEase(witchAnimEaseXEase);
        witch.DOAnchorPosY(witchAnimTargetPos.y, witchAnimLength).SetDelay(timeToStartTransition).SetEase(witchAnimEaseYEase);
        witch.DOSizeDelta(witchAnimSize, witchAnimLength).SetDelay(timeToStartTransition).SetEase(witchAnimEaseSizeEase);

        overlay.DOColor(Color.black, fadeAnimLength).SetEase(fadeAnimEase).SetDelay(fadeAnimDelay).OnComplete(() =>
        {

            float delay = thunderDelays;
            foreach(var thunder in thunders)
            {
                DOVirtual.DelayedCall(delay, ()=>
                {
                    thunder.SetActive(true);
                    overlay.DOColor(Color.clear, fadeThunderBlackAnimLength).SetEase(fadeThunderBlackAnimCurve);
                });
                delay += thunderDelays;
            }


            overlay.DOColor(Color.clear, fadeAnimLength).SetDelay(delay).SetEase(fadeAnimEase).OnStart(() =>
            {
                List<Vector2> housesPos = new List<Vector2>();
                List<Vector2> housesSize = new List<Vector2>();
                foreach(var uiHouse in uiHouses)
                {
                    housesPos.Add(uiHouse.position);
                    housesSize.Add(uiHouse.size);
                }

                List<Vector2> randomizedHousesPos = new List<Vector2>();
                List<Vector2> randomizedHousesSize = new List<Vector2>();
                do
                {
                    int index = Random.Range(0, housesPos.Count);
                    randomizedHousesPos.Add(housesPos[index]);
                    randomizedHousesSize.Add(housesSize[index]);
                    housesPos.RemoveAt(index);
                    housesSize.RemoveAt(index);
                }while (housesPos.Count > 0);

                int posIndex = 0;
                foreach(var uiHouse in uiHouses)
                {
                    uiHouse.house.anchoredPosition = randomizedHousesPos[posIndex];
                    uiHouse.position = randomizedHousesPos[posIndex];

                    uiHouse.house.sizeDelta = randomizedHousesSize[posIndex];
                    uiHouse.size = randomizedHousesSize[posIndex];

                    uiHouse.house.GetComponent<Image>().sprite = uiHouse.nightHouseSprite;

                    if ((posIndex%2 == 0 && randomizedHousesPos[posIndex].x > 0f) ||
                        (posIndex%2 != 0 && randomizedHousesPos[posIndex].x < 0f))
                    {
                        uiHouse.house.localScale = new Vector3(-1, 1f, 1f);
                    }

                    posIndex++;
                }

                matchingContainer.SetActive(true);
                gameplayBackground.sprite = nightBackgroundSprite;

                //Sets costumes functionality
                int costumeIndex = 0;
                foreach(var uiCostume in uICostumes)
                {
                    int cacheCostumeIndex = costumeIndex;
                    uiCostume.button.onClick.AddListener(() =>
                    {
                        int randomKittySgxIndex = Random.Range(0, kittyMeowSFX.Length);
                        audioSourceSFX.PlayOneShot(kittyMeowSFX[randomKittySgxIndex]);
                        if (selectedCostume == uiCostume.name)
                        {
                            // uiCostume.image.color = Color.white;
                            selectedCostume = StyleName.None;
                        }
                        else
                        {
                            // uICostumes[selectedCostumeIndex].image.color = Color.white;
                            // uiCostume.image.color = Color.green;
                            selectedCostume = uiCostume.name;
                            selectedCostumeIndex = cacheCostumeIndex;
                        }
                    });

                    costumeIndex++;
                }

            }).OnComplete(() =>
            {
                overlay.raycastTarget = false;

            });
        });
    }

    public void HouseClicked(StyleName houseStyle)
    {
        audioSourceSFX.PlayOneShot(houseClickedSFX);
        if (currentPhase == Phase.Investigation && initialDialoge == false)
        {
            ShowHouse(houseStyle);
        }
        else
        {
            int uiHouseIndex = uiHouses.FindIndex(h => h.style == houseStyle);

            submitButton.SetActive(false);
            // Debug.Log(uiHouses[uiHouseIndex].costumeStyle);
            if(uiHouses[uiHouseIndex].costumeStyle != StyleName.None)
            {
                var uiCostume = uICostumes.Find(c => c.name == uiHouses[uiHouseIndex].costumeStyle);
                uiCostume.image.gameObject.SetActive(true);
                uiHouses[uiHouseIndex].costumeStyle = StyleName.None;
                uiHouses[uiHouseIndex].costumeImage.color = Color.clear;
                // Debug.Log($"Removing {houseStyle}, {selectedCostume}");
            }

            // Debug.Log(selectedCostume);
            if (selectedCostume != StyleName.None)
            {
                // Debug.Log($"Adding {houseStyle}, {selectedCostume}");

                uiHouses[uiHouseIndex].costumeStyle = selectedCostume;
                uiHouses[uiHouseIndex].costumeImage.color = Color.white;
                uiHouses[uiHouseIndex].costumeImage.sprite = uICostumes[selectedCostumeIndex].image.sprite;
                uiHouses[uiHouseIndex].costumeRect.DOKill();
                uiHouses[uiHouseIndex].costumeRect.DOPunchScale(Vector3.one * 2f, .5f, 0);

                uICostumes[selectedCostumeIndex].image.gameObject.SetActive(false);
                // uICostumes[selectedCostumeIndex].image.color = Color.white;
                selectedCostume = StyleName.None;

                bool allCostumesUsed = uICostumes.TrueForAll(c => !c.image.gameObject.activeSelf);

                if (allCostumesUsed)
                    submitButton.SetActive(true);
            }
        }
    }

    public void ShowScore()
    {
        int score = 0;

        foreach(var uiHouse in uiHouses)
        {
            // Debug.Log($"{uiHouse.objsStyles == null}");
            int matchCounter = uiHouse.objsStyles.FindAll(objStyle => objStyle == uiHouse.costumeStyle).Count;
            // Debug.Log(matchCounter);

            if (matchCounter >= 3)
                score += data.scorePerfect;
            else if (matchCounter > 0)
                score += data.scoreClose;
        }

        // score = 3000;
        int pawScore = (data.scorePerfect * uiHouses.Count) / paws.Length;
        // Debug.Log($"{score}, {pawScore}");
        for (int i=0 ; i<paws.Length ; i++)
        {
            if (score < pawScore * (i + 1))
                break;

            paws[i].rt.DOPunchScale(Vector3.one * 1.5f, 1f, 0, 0f).SetEase(Ease.OutExpo)
                .SetDelay(pawDelay * i).OnStart(() => paws[i].image.sprite = paws[i].completedSprite);
        }

        audioSourceSFX.PlayOneShot(scoreSFX);
        scoreText.DOText(score.ToString(), 3f, false, ScrambleMode.Numerals).SetEase(Ease.OutExpo);
        scorePanel.SetActive(true);
    }


    #region UI
    public void Quit() => Application.Quit();

    public void Restart(bool _showMainMenu)
    {
        showMainMenu = _showMainMenu;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    #endregion
}
