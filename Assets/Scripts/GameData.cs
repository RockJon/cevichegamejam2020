﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[CreateAssetMenu(fileName = "GameData", menuName = "ScriptableObjects/GameData", order = 1)]
public class GameData : ScriptableObject
{
    public int maxObjsPerHouse = 5, mainStyleObjs = 3, maxCheckableObjs = 20;
    public int scoreClose = 200, scorePerfect = 500;

    public List<Style> styles;
}

public enum StyleName
{
    None = -1, Nonna = 0, YogaLover = 1, DragQueen = 2, Kawaii = 3, Darks = 4, Paleontology = 5
}


[Serializable]
public class StyleObject
{
    [HideInInspector]
    public StyleName styleName;

    public Sprite sprite;
    public string title;
    public string description;
}


[Serializable]
public class Style
{
    public StyleName name;
    public Sprite background;
    public List<StyleObject> objects;
    public List<Vector3> objectsPositions;

    public void SetObjsStyleName()
    {
        for (int i=0 ; i<objects.Count ; i++)
        {
            objects[i].styleName = name;
        }
    }
}

